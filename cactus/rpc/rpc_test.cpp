#include <cactus/test.h>

#include <cactus/rpc/rpc_test.pb.h>

using namespace cactus;

const folly::SocketAddress kTestAddress("127.0.0.1", 15860);

class TestServiceDummy : public TestServiceHandler {
public:
    void Search(const TestRequest& request, TestResponse* response) override {
        response->set_results(request.query());
    }

    void Ping(const PingRequest& request, PingResponse* response) override {
        throw std::runtime_error("pong");
    }
};

FIBER_TEST_CASE("Simple call") {
    TestServiceDummy dummy;
    SimpleRpcServer server(kTestAddress);
    server.Register(&dummy);

    ServerGroup g;
    g.Spawn([&] { server.Serve(); });

    SimpleRpcChannel channel(kTestAddress);
    TestServiceClient client(&channel);

    TestRequest search_request;
    search_request.set_query("пластиковые окна");
    auto search_response = client.Search(search_request);
    REQUIRE("пластиковые окна" == search_response->results());

    REQUIRE_THROWS_AS(client.Ping(PingRequest{}), RpcCallError);
}