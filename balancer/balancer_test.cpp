#include <cactus/test.h>

#include <cactus/cactus.h>

#include "balancer.h"

const folly::SocketAddress kLocalHost("127.0.0.1", 0);

const folly::SocketAddress kBalancerAddr("127.0.0.1", 25000);

std::vector<folly::SocketAddress> MakePeerAddresses(int first_port, int count) {
    std::vector<folly::SocketAddress> res;
    res.reserve(count);
    for (int port = first_port; port < first_port + count; ++port) {
        res.push_back(kLocalHost);
        res.back().setPort(port);
    }
    return res;
}

void RunSquaringPeers(
    cactus::ServerGroup& group,
    const std::vector<folly::SocketAddress>& addresses,
    std::vector<int>& connection_counts,
    int dead_peer_index = -1)
{
    for (int i = 0; i < (int)addresses.size(); ++i) {
        auto& connection_count = connection_counts[i];
        if (i == dead_peer_index) {
            continue;
        }
        std::shared_ptr<cactus::IListener> listener = cactus::ListenTCP(addresses[i]);
        group.Spawn([listener, &connection_count] () {
            cactus::ServerGroup group;
            while (true) {
                std::shared_ptr<cactus::IConn> conn = listener->Accept();
                ++connection_count;
                group.Spawn([conn] {
                    int num;
                    conn->ReadFull(cactus::View(&num, sizeof(num)));
                    num *= num;
                    conn->Write(cactus::View(&num, sizeof(num)));
                });
            }
        });
    }
}

FIBER_TEST_CASE("Simple") {
    constexpr int kClientConnectionCount = 10;

    auto peers = MakePeerAddresses(26000, 10);

    cactus::ServerGroup group;
    std::vector<int> connection_counts(peers.size(), 0);
    RunSquaringPeers(group, peers, connection_counts);

    auto server = CreateBalancer(kBalancerAddr);
    server->SetBackends(peers);
    server->Run();

    cactus::SleepFor(std::chrono::seconds(1));

    std::vector<std::unique_ptr<cactus::IConn>> conns;
    for (int j = 0; j < kClientConnectionCount; ++j) {
        conns.push_back(cactus::DialTCP(kBalancerAddr));
        conns.back()->Write(cactus::View(&j, sizeof(j)));
        int rsp;
        conns.back()->ReadFull(cactus::View(&rsp, sizeof(rsp)));
        REQUIRE(rsp == j * j);
    }

    REQUIRE(connection_counts == std::vector<int>(peers.size(), kClientConnectionCount / peers.size()));
}

FIBER_TEST_CASE("DeadPeer") {
    constexpr int kClientConnectionCount = 100;
    constexpr int kDeadPeerIndex = 4;

    auto peers = MakePeerAddresses(26000, 10);

    cactus::ServerGroup group;

    std::vector<int> connection_counts(peers.size(), 0);
    RunSquaringPeers(group, peers, connection_counts, kDeadPeerIndex);

    auto server = CreateBalancer(kBalancerAddr);
    server->SetBackends(peers);
    server->Run();

    cactus::SleepFor(std::chrono::seconds(1));

    std::vector<std::unique_ptr<cactus::IConn>> conns;
    for (int j = 0; j < kClientConnectionCount; ++j) {
        conns.push_back(cactus::DialTCP(kBalancerAddr));
        conns.back()->Write(cactus::View(&j, sizeof(j)));
        int rsp;
        conns.back()->ReadFull(cactus::View(&rsp, sizeof(rsp)));
        REQUIRE(rsp == j * j);
    }

    for (int i = 0; i < (int)peers.size(); ++i) {
        if (i == kDeadPeerIndex) {
            REQUIRE(connection_counts[i] == 0);
        } else {
            REQUIRE(connection_counts[i] >= kClientConnectionCount / (peers.size() - 1));
            REQUIRE(connection_counts[i] <= kClientConnectionCount / (peers.size() - 1) + 1);
        }
    }
}

FIBER_TEST_CASE("Reconfiguration") {
    constexpr int kClientConnectionCount = 100;

    auto initial_peers = MakePeerAddresses(26000, 10);
    auto secondary_peers = MakePeerAddresses(26100, 5);

    cactus::ServerGroup group;

    std::vector<int> initial_connection_counts(initial_peers.size(), 0);
    RunSquaringPeers(group, initial_peers, initial_connection_counts);

    std::vector<int> secondary_connection_counts(secondary_peers.size(), 0);
    RunSquaringPeers(group, secondary_peers, secondary_connection_counts);

    auto server = CreateBalancer(kBalancerAddr);
    server->SetBackends(initial_peers);
    server->Run();

    cactus::SleepFor(std::chrono::seconds(1));

    auto run_check = [&] (int peer_count, const std::vector<int>& connection_counts) {
        std::vector<std::unique_ptr<cactus::IConn>> conns;
        for (int j = 0; j < kClientConnectionCount; ++j) {
            conns.push_back(cactus::DialTCP(kBalancerAddr));
            conns.back()->Write(cactus::View(&j, sizeof(j)));
            int rsp;
            conns.back()->ReadFull(cactus::View(&rsp, sizeof(rsp)));
            REQUIRE(rsp == j * j);
        }

        REQUIRE(connection_counts == std::vector<int>(peer_count, kClientConnectionCount / peer_count));
    };

    run_check(initial_peers.size(), initial_connection_counts);

    server->SetBackends(secondary_peers);

    run_check(secondary_peers.size(), secondary_connection_counts);
}
