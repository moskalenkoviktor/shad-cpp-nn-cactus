#pragma once

#include <cactus/cactus.h>

// Socks4Connect посылает в conn запрос CONNECT и принимает ответ от прокси сервера.
//
// Функция бросает исключение в случае ошибки.
void Sock4Connect(cactus::IConn* conn, folly::SocketAddress endpoint, const std::string& user);

struct Proxy {
    folly::SocketAddress proxy_address;
    std::string username;
};

// DialProxyChain подключается к endpoint через цепочку proxies.
std::unique_ptr<cactus::IConn> DialProxyChain(std::vector<Proxy> proxies, folly::SocketAddress endpoint);